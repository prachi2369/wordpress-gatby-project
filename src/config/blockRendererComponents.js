import React from "react";
import {
  BlockRenderer,
  getClasses,
  getStyles,
} from "@webdeveducation/wp-block-tools";
import { MediaText } from "../components/MediaText/MediaText";
import CallToActionButton from "../components/CallToActionButton/CallToActionButton";
import { GatsbyImage } from "gatsby-plugin-image";
import { Cover } from "../components";
import numeral from "numeral";
import CarSearch from "../components/CarSearch/CarSearch";
const blockRendererComponents = (block) => {
  switch (block.name) {
    case "tgg/carsearch": {
      return (
        <CarSearch
          key={block.id}
          style={getStyles(block)}
          className={getClasses(block)}
        />
      );
    }
    case "tgg/carprice": {
      return (
        <div className="flex justify-center">
          <div className="bg-black px-5 py-8 font-heading text-3xl text-white">
            £{numeral(block.attributes.price).format("0,0")}
          </div>
        </div>
      );
    }
    case "core/Cover": {
      console.log(">>>COVER BLOCK", block);
      return (
        <Cover
          key={block.id}
          style={getStyles(block)}
          className={getClasses(block)}
          gatsbyImage={block.attributes.gatsbyImage}
        >
          <BlockRenderer blocks={block.innerBlocks} />
        </Cover>
      );
    }
    case "core/image": {
      return (
        <figure key={block.id} className={getClasses(block)}>
          <GatsbyImage
            style={getStyles(block)}
            image={block.attributes.gatsbyImage}
            alt={block.attributes.alt || ""}
            width={block.attributes.width}
            height={block.attributes.height}
          />
        </figure>
      );
    }
    case "tgg/ctabutton": {
      const alignMap = {
        left: "text-left",
        center: "text-center",
        right: "text-right",
      };
      console.log(">>>>>cta btn", block);
      return (
        <div key={block.id} className={alignMap["center"]}>
          {/* label -  block.attributes.data.label , destination - block.attributes.data.destination */}
          <CallToActionButton
            destination={block.attributes.data.destination}
            label={block.attributes.data.label}
          />
        </div>
      );
    }
    case "core/media-text": {
      return (
        <MediaText
          key={block.id}
          className={getClasses(block)}
          style={getStyles(block)}
          verticalAlignment={block.attributes.verticalAlignment}
          gatsbyImage={block.attributes.gatsbyImage}
          mediaPosition={block.attributes.mediaPosition}
        >
          <BlockRenderer blocks={block.innerBlocks} />
        </MediaText>
      );
    }
    default:
      return null;
  }
};
export default blockRendererComponents;
