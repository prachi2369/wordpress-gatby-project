import React from "react";
import { useQuery, gql } from "@apollo/client";
import numeral from "numeral";
import { navigate } from "gatsby";
const CarSearch = ({ style, className }) => {
  const pageSize = 3;
  let page = 1;
  let defaultMaxPrice = "";
  let defaultMinPrice = "";
  let defaultColor = "";

  if (typeof window !== "undefined") {
    const params = new URLSearchParams(window.location.search);
    page = parseInt(params.get("page") || "1");
    defaultMaxPrice = params.get("maxPrice");
    defaultMinPrice = params.get("minPrice");
    defaultColor = params.get("color");
  }

  let metaQuery = "{}";
  if (defaultColor || defaultMaxPrice || defaultMinPrice) {
    let colorQuery = "";
    let minPriceQuery = "";
    let maxPriceQUERY = "";
    if (defaultColor) {
      colorQuery = `{key: "color", compare: EQUAL_TO, value: "${defaultColor}"},`;
    }
    if (defaultMinPrice) {
      minPriceQuery = `{key: "price", compare: GREATER_THAN_OR_EQUAL_TO, type: NUMERIC, value: "${defaultMinPrice}"},`;
    }
    if (defaultMaxPrice) {
      maxPriceQUERY = `{key: "price", compare: LESS_THAN_OR_EQUAL_TO, type: NUMERIC, value: "${defaultMaxPrice}"},`;
    }
    metaQuery = `{
      relation: AND
      metaArray: [${colorQuery}${minPriceQuery}${maxPriceQUERY}]
    }`;
  }
  const { data, loading, error } = useQuery(gql`
    query CarQuery {
      cars(where: {metaQuery: ${metaQuery}}) {
        nodes {
          databaseId
          uri
          title
          featuredImage {
            node {
              sourceUrl(size: LARGE)
            }
          }
          carDetails {
            price
            color
          }
        }
      }
    }
  `);
  console.log(">>>data,", data);

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const params = new URLSearchParams(formData);
    params.set("page", "1");
    navigate(`${window.location.pathname}?${params.toString()}`);
  };
  return (
    <div className={className} style={style}>
      <fieldset>
        <form
          onSubmit={handleSubmit}
          className="mb-4 grid grid-cols-1 gap-4 bg-stone-200 p-4 md:grid-cols-[1fr_1fr_1fr_110px]"
        >
          <div>
            <strong>Min Price</strong>
            <input
              type="number"
              name="minPrice"
              defaultValue={defaultMinPrice}
            />
          </div>
          <div>
            <strong>Max Price</strong>
            <input
              type="number"
              name="maxPrice"
              defaultValue={defaultMaxPrice}
            />
          </div>
          <div>
            <strong>Color</strong>
            <select name="color" defaultValue={defaultColor}>
              <option value="">Any color</option>
              <option value="red">Red</option>
              <option value="white">White</option>
              <option value="green">Green</option>
            </select>
          </div>
          <div className="flex">
            <button type="submit" className="btn mt-auto mb-[2px]">
              Submit
            </button>
          </div>
        </form>
      </fieldset>
      {!loading && !!data?.cars?.nodes?.length && (
        <div className="grid grid-cols-1 gap-4 md:grid-cols-3">
          {data.cars.nodes.map((car) => (
            <div
              key={car.databaseId}
              className="flex flex-col border border-stone-100 border-stone-200 bg-stone-50"
            >
              {!!car.featuredImage?.node?.sourceUrl && (
                <img
                  src={car.featuredImage?.node?.sourceUrl}
                  alt=""
                  className="h-[200px] w-full object-cover"
                />
              )}
              <div>
                {" "}
                <div>{car.title}</div>
                <div>£{numeral(car.carDetails.price).format("0,0")}</div>
                <div>---{car.carDetails.color}</div>
              </div>
            </div>
          ))}{" "}
        </div>
      )}
    </div>
  );
};
export default CarSearch;
