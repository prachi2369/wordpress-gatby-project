import { Link } from "gatsby";
import React from "react";
const CallToActionButton = ({ label, destination }) => {
  return (
    <Link to={destination} className="btn">
      {label}
    </Link>
  );
};
export default CallToActionButton;
