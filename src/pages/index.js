import * as React from "react";

const IndexPage = () => {
  return <main>This is home page</main>;
};

export default IndexPage;

export const Head = () => (
  <>
    <meta name="description" content="this is page description"></meta>
    <title>Home Page</title>
  </>
);
