import React from "react";
import { BlockRendererProvider } from "@webdeveducation/wp-block-tools";
import blockRendererComponents from "../config/blockRendererComponents";
import { Link } from "gatsby";
import Layout from "../components/Layout/Layout";
const Page = (props) => {
  return (
    <Layout>
      <BlockRendererProvider
        allBlocks={props.pageContext.blocks}
        siteDomain={process.env.GATSBY_WP_URL}
        renderComponent={blockRendererComponents}
        customInternalLinkComponent={(
          { children, internalHref, className },
          index
        ) => {
          return (
            <Link to={internalHref} className={className} key={index}>
              {children}
            </Link>
          );
        }}
      />
    </Layout>
  );
};
export default Page;
