const path = require("path");
const {
  assignIds,
  assignGatsbyImage,
} = require("@webdeveducation/wp-block-tools");
const fs = require("fs");
exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;
  const pageTemplate = path.resolve("src/templates/page.js");
  const { data } = await graphql(`
    query allPagesQuery {
      wp {
        themeStylesheet
      }
      allWpCar {
        nodes {
          databaseId
          uri
          blocks
        }
      }
      allWpPage {
        nodes {
          databaseId
          uri
          blocks
        }
      }
    }
  `);

  try {
    fs.writeFileSync("./public/themeStylesheet.css", data.wp.themeStylesheet);
  } catch (error) {}

  const allPages = [...data.allWpPage.nodes, ...data.allWpCar.nodes];

  for (let i = 0; i < allPages.length; i++) {
    const page = allPages[i];
    let blocks = page.blocks;
    blocks = assignIds(blocks);
    blocks = await assignGatsbyImage({
      blocks,
      graphql,
      coreMediaText: true,
      coreImage: true,
      coreCover: true,
    });
    createPage({
      path: page.uri,
      component: pageTemplate,
      context: {
        blocks,
      },
    });
  }
};
